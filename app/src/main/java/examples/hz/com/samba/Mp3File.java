package examples.hz.com.samba;

import android.util.Log;

import java.util.ArrayList;

import mpc.MPCSong;

/**
 * Created by anton on 10-1-16.
 */
public class Mp3File {
    private int time;
    private String file;
    private String title;
    private String album;
    private String artist;
    private int tracknr;
    private MPCSong mpcSong;
    private int year;
    public Mp3File(ArrayList<String> s) {
        for (int i=0;i<s.size();i++){
            if (s.get(i).startsWith("file:")){
                this.setFile(s.get(i).split("file:")[1].trim());
            }
            if (s.get(i).startsWith("Artist:")){
                this.setArtist(s.get(i).split("Artist:")[1].trim());
            }
            if (s.get(i).startsWith("Album:")){
                this.setAlbum(s.get(i).split("Album:")[1].trim());
            }
            if (s.get(i).startsWith("Title:")){
                this.setTitle(s.get(i).split("Title:")[1].trim());
            }
            if (s.get(i).startsWith("Date:")){
                this.setYear(Integer.parseInt(s.get(i).split("Date:")[1].trim()));
            }
            if (s.get(i).startsWith("Track:")){
                this.setTracknr(Integer.parseInt(s.get(i).split("Track:")[1].split("/")[0].trim()));
            }
            if (s.get(i).startsWith("Time:")){
                this.setTime(Integer.parseInt(s.get(i).split("Time:")[1].trim()));
            }
        }

    }
    public Mp3File(String s){
        String[] hs=s.split("=== ");
        this.setFile(hs[0].split("/home/wieneke/FamilyLibrary/FamilyMusic/")[1]);
        for (int i=1;i<hs.length;i++){
            //Log.v("samba",hs[i]);
            ///home/wieneke/FamilyLibrary/
            if(hs[i].startsWith("TIT2")){
                this.setTitle(hs[i].split("TIT2")[1].trim());
            }
            if(hs[i].startsWith("TALB")){
                this.setAlbum(hs[i].split("TALB")[1].trim());
            }
            if(hs[i].startsWith("TPE1")){
                this.setArtist(hs[i].split("TPE1")[1].trim());
            }
            if(hs[i].startsWith("TRCK")){
                this.setTracknr(Integer.parseInt(hs[i].split("TRCK")[1].split("/")[0].trim()));
            }
            if(hs[i].startsWith("TYER")){
                this.setYear(Integer.parseInt(hs[i].split("TYER")[1].trim()));
            }
            if(hs[i].startsWith("TIME")){
                this.setTime(Integer.parseInt(hs[i].split("TIME")[1].trim()));
            }
        }
        this.setMpcSong(new MPCSong(file, 0, artist, title, album, tracknr));
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public int getTracknr() {
        return tracknr;
    }

    public void setTracknr(int tracknr) {
        this.tracknr = tracknr;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public MPCSong getMpcSong() {
        return mpcSong;
    }

    public void setMpcSong(MPCSong mpcSong) {
        this.mpcSong = mpcSong;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }
    public String getTimeNice(){
        int min=time/60;
        int sec=time-min*60;
        return String.format("%02d:%02d", min,sec);
    }
}
